# Базовая установка и конфигурация PostgreSQL
Данная роль устанавливает PostgreSQL версий: 10,11,12.\
**В данный момент УАБД поддерживает версии PostgreSQL до 11 версии включительно.**

Установка возможна из локального каталога roles/postgresql/files (пакеты для установки версий 10,11,12 уже присутствуют) или из репозитория.

## Использование роли

Для базовой установки достаточно указать версию PostgreSQL, для этого в корне есть файл postgresql.yml, где в переменной pgdb можно задать следующие значения соответствующие версии СУБД: v10, v11, v12.\
Пример файла postgresql.yml с указанной переменной pgdb для установки PostgreSQL версии 11:
>---\
>\- hosts: all <br /> 
>  vars_files:<br /> 
>    \- "roles/postgresql/vars/vars.yml"\
>  roles:\
>    \- role: postgresql\   
>       pgdb: v11

Также в корне есть inventory-fail.yml, можно отредактировать его или использовать свой инвентори файл.
Пример:
> all:\
>  hosts:\
>    remote:\
>      ansible_host: remote_host\
>      remote_user: remote_ivan\
>    local:\
>      ansible_host: 127.0.0.1\
>      ansible_user: root

Запускается плэйбук так: ansible-playbook postgresql.yml -i <путь к инвентори> .
Если необходимо указать ключи и дополнительные переменные, то так: ansible-playbook postgresql.yml -i <путь к инвентори> -e <дополнительные переменные> --private-key <путь/файл с приватным ssh ключом>
Пример:
> ansible-playbook postgresql.yml -i inventory-file.yml 

### Дополнительная конфигурация (опционально)
Если нужно дополнительно создать базы данных, создать пользователей и выдать им права, создать схему для базы данных или изменить доступы в pg_hba.conf, то необходимо в файле roles/postgresql/vars/vars.yml прописать нужные переменные:
Пример переменных для создания нескольких бд:
>    db:\
>      db-1:\
>        db_name: db-1\
>        owner_name:\
>        db_template: template0<br /> 
>        encod: UTF-8\
>        locate: ru_RU.UTF-8\
>      db-2:\
>        db_name: db-2\
>        owner_name:<br /> 
>        db_template: template0\
>        encod: UTF-8\
>        locate: ru_RU.UTF-8

Пример переменных для создания схемы для бд:
>    schema_name: db-1-schema\
>    db_name:  db-1\
>    schema_owner:

Пример переменных для создания нескольких пользователей:
>    users:\
>      user-1:\
>        role_name: test-user-1\
>        role_pw: SuperStrongPassWord20\
>        role_attributes: SUPERUSER\
>        data_base: db-1\
>        priv_db: ALL\
>        db_schema_name: db-1-schema\
>        priv_tb: ALL\
>      user-2:\
>        role_name: test-user-2\
>        role_pw: SuperStrongPassWord21\
>        role_attributes: NOCREATEDB,NOCREATEROLE,NOSUPERUSER\
>        data_base: db-1\
>        priv_db:\
>        db_schema_name:<br /> 
>        priv_tb: SELECT

Пример переменных для добавления строк в pg_hba.conf:
>    conf_strings:\
>      string-1:\
>        type_of_rule: host\
>        target_db: all\
>        user_pg_hba: all<br /> 
>        address: 0.0.0.0/0\
>        auth_method: md5\
>      string-2:\
>        type_of_rule: host\
>        target_db: all\
>        user_pg_hba: all<br /> 
>        address: 127.0.0.2/32<br /> 
>        auth_method: ident

В данный момент в файле vars.yml прописаны перменные для создания пароля пользователя postgres:
Пример:
>    login_postgres: postgres\
>    postgres_pw: postgres

И переменные для создания дефолтной бд. 

Сами дополнительные задачи находятся в roles/postgresql/tasks/enable-service.yml. 
Пример: 
>\- name: "Config bd create create bd"\
>   include_tasks: config_db/create-some-db.yml\
>   when: db is defined

Можно также добавить собственные задачи на примере существующих и затем занести их в roles/postgresql/tasks/enable-service.yml.

### Установка из репозитория (опционально)
Установить пакеты из репозитория возможно при указании переменных r_fqdn и r_ip после переменной pgdb в postgresql.yml.
> ...\
>pgdb: v11\
>r_fqdn: "http://lr24-101lv.vtb24.ru/"<br />
>r_ip: "http://10.64.36.176/"<br />
> ...

Пути до пакетов и GPG-ключа задаются в roles/postgresql/add-repo-pckg-instl.yml
>...\
>baseurl: "{{ r_fqdn }}other/postgres{{ pgdb | replace('v','') }}_el7/ {{ r_ip }}other/postgres{{ pgdb | replace('v','') }}_el7/"\
>gpgkey: "{{ r_fqdn }}other/postgres{{ pgdb | replace('v','') }}_el7/RPM-GPG-KEY-PGDG"\
>...

